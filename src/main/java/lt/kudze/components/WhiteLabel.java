package lt.kudze.components;

import javax.swing.*;
import java.awt.*;

public class WhiteLabel extends JLabel {
    public static final Color WHITE = new Color(255, 255, 255);

    public WhiteLabel(String contents) {
        super(contents);
        this.setForeground(WHITE);
    }

}
