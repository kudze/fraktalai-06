package lt.kudze.components;

import javax.swing.*;
import java.awt.*;

public class RedLabel extends JLabel {
    public static final Color WHITE = new Color(255, 0, 0);

    public RedLabel(String contents) {
        super(contents);
        this.setForeground(WHITE);
    }

}
