package lt.kudze.factory;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ButtonFactory {
    public static JComponent makeButton(String title, ActionListener actionListener) {
        JButton button = new JButton(title);
        button.addActionListener(actionListener);
        return button;
    }
}
