package lt.kudze.data;

import java.util.ArrayList;
import java.util.List;

//6.3 f_a(x) = ax(1 + c(1 - x))
public class DynamicSystem {

    public static double calculate(double a, double c, double x) {
        //return a * x * (1 - x);
        return a * x * (1 + c * (1 - x));
    }

    public static List<Double> makeSeries(double a, double c, double x, int till) {
        if (till < 0) throw new IllegalArgumentException("Till should be more or equal to zero!");

        ArrayList<Double> result = new ArrayList<>(till + 1);
        result.add(x);

        for (int i = 1; i <= till; i++) {
            x = calculate(a, c, x);
            result.add(x);
        }

        return result;
    }

}
