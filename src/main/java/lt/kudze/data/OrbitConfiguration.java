package lt.kudze.data;

public class OrbitConfiguration {
    protected double x0;
    protected double a;
    protected double c;
    protected int from;
    protected int till;

    public OrbitConfiguration() {
        this(1, -1, 0.1, 0, 100);
    }

    public OrbitConfiguration(double x0, double a, double c, int from, int till) {
        this.x0 = x0;
        this.a = a;
        this.c = c;
        this.from = from;
        this.till = till;
    }

    public double getX0() {
        return x0;
    }

    public double getA() {
        return a;
    }

    public double getC() {
        return c;
    }

    public int getFrom() {
        return from;
    }

    public int getTill() {
        return till;
    }
}
