package lt.kudze.data;

public class TreeConfiguration {
    protected double x0;
    protected double aFrom;
    protected double aTill;
    protected double c;

    public TreeConfiguration() {
        this(1, 1, 2, 1);
    }

    public TreeConfiguration(double x0, double aFrom, double aTill, double c) {
        this.x0 = x0;
        this.aFrom = aFrom;
        this.aTill = aTill;
        this.c = c;
    }

    public double getX0() {
        return x0;
    }

    public double getAFrom() {
        return aFrom;
    }

    public double getATill() {
        return aTill;
    }

    public double getC() {
        return c;
    }
}
