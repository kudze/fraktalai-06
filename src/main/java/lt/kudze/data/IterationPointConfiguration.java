package lt.kudze.data;

public class IterationPointConfiguration {
    protected double x0;
    protected double a;
    protected double c;
    protected int lineCount;

    public IterationPointConfiguration() {
        this(0.2, 1, 1, 50);
    }

    public IterationPointConfiguration(double x0, double a, double c, int lineCount) {
        this.x0 = x0;
        this.a = a;
        this.c = c;
        this.lineCount = lineCount;
    }

    public double getX0() {
        return x0;
    }

    public double getA() {
        return a;
    }

    public double getC() {
        return c;
    }

    public int getLineCount() {
        return lineCount;
    }
}
