package lt.kudze.panels;

import lt.kudze.components.WhiteLabel;

import javax.swing.*;

public class InputPanel extends GrayVerticalPanel {

    public InputPanel(String label, JTextField input, JLabel validationLabel) {
        this.add(new WhiteLabel(label));
        this.add(input);
        this.add(validationLabel);
    }

}
