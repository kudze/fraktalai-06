package lt.kudze.panels;

import javax.swing.*;
import java.awt.*;

public abstract class OrangePanel extends JPanel {
    public static final Color BG_ORANGE = new Color(255, 127, 80);

    public OrangePanel() {
        this.setBackground(BG_ORANGE);
    }
}
