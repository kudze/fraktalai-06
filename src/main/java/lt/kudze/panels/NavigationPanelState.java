package lt.kudze.panels;

public enum NavigationPanelState {
    ORBIT,
    ITERATE_POINT,
    TREE
}
