package lt.kudze.panels;

import lt.kudze.Application;
import lt.kudze.factory.ButtonFactory;
import lt.kudze.panels.iterationPoint.IterationPointPanel;
import lt.kudze.panels.orbit.OrbitPanel;
import lt.kudze.panels.tree.TreePanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NavigationPanel extends GrayPanel {
    protected NavigationPanelState state = NavigationPanelState.ORBIT;
    protected GrayPanel navigation = new GrayPanel();

    public NavigationPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        navigation.add(ButtonFactory.makeButton("Orbitu grafikas", new NavigateAction(this, NavigationPanelState.ORBIT)));
        navigation.add(ButtonFactory.makeButton("Funkcijos iteruojamo taško grafinis vaizdas", new NavigateAction(this, NavigationPanelState.ITERATE_POINT)));
        navigation.add(ButtonFactory.makeButton("Feigenbaumo medis", new NavigateAction(this, NavigationPanelState.TREE)));

        this.addChildren();
    }

    protected void addChildren() {
        this.add(navigation);

        switch (state) {
            case ORBIT:
                this.add(new OrbitPanel());
                break;
            case ITERATE_POINT:
                this.add(new IterationPointPanel());
                break;
            case TREE:
                this.add(new TreePanel());
                break;
        }
    }

    public void setState(NavigationPanelState state) {
        if (this.state == state)
            return;

        this.state = state;
        this.removeAll();
        this.addChildren();

        this.revalidate();
        this.repaint();

        Application.getInstance().getWindow().pack();
    }
}

record NavigateAction(NavigationPanel panel, NavigationPanelState state) implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        panel.setState(state);
    }
}