package lt.kudze.panels.orbit;

import lt.kudze.panels.GrayPanel;
import lt.kudze.panels.OrangePanel;

import javax.swing.*;

public class OrbitPanel extends GrayPanel {
    protected OrbitConfigurePanel orbitConfigurePanel;

    public OrbitPanel() {
        this.orbitConfigurePanel = new OrbitConfigurePanel(this);

        this.addChild();
    }

    public void onConfigurationUpdate() {
        this.removeAll();
        this.addChild();

        this.revalidate();
        this.repaint();
    }

    protected void addChild() {
        this.add(orbitConfigurePanel);
        this.add(new OrbitLineChartPanel(orbitConfigurePanel.getOrbitConfiguration()));
    }

    public OrbitConfigurePanel getOrbitConfigurePanel() {
        return orbitConfigurePanel;
    }
}
