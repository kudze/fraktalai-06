package lt.kudze.panels.orbit;

import lt.kudze.Application;
import lt.kudze.components.RedLabel;
import lt.kudze.components.WhiteLabel;
import lt.kudze.data.OrbitConfiguration;
import lt.kudze.factory.ButtonFactory;
import lt.kudze.panels.GrayPanel;
import lt.kudze.panels.InputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrbitConfigurePanel extends GrayPanel {
    protected OrbitPanel orbitPanel;
    protected OrbitConfiguration orbitConfiguration = new OrbitConfiguration();

    protected JTextField x0Field = new JTextField(String.valueOf(this.orbitConfiguration.getX0()));
    protected JTextField aField = new JTextField(String.valueOf(this.orbitConfiguration.getA()));
    protected JTextField cField = new JTextField(String.valueOf(this.orbitConfiguration.getC()));
    protected JTextField fromField = new JTextField(String.valueOf(this.orbitConfiguration.getFrom()));
    protected JTextField tillField = new JTextField(String.valueOf(this.orbitConfiguration.getTill()));
    protected JLabel x0ValidationLabel = new RedLabel("");
    protected JLabel aValidationLabel = new RedLabel("");
    protected JLabel cValidationLabel = new RedLabel("");
    protected JLabel fromValidationLabel = new RedLabel("");
    protected JLabel tillValidationLabel = new RedLabel("");

    OrbitConfigurePanel(OrbitPanel orbitPanel) {
        this.orbitPanel = orbitPanel;

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(new WhiteLabel("Konfiguracija"));
        this.add(new InputPanel("x0", x0Field, x0ValidationLabel));
        this.add(new InputPanel("a", aField, aValidationLabel));
        this.add(new InputPanel("c", cField, cValidationLabel));
        this.add(new InputPanel("nuo", fromField, fromValidationLabel));
        this.add(new InputPanel("iki", tillField, tillValidationLabel));
        this.add(ButtonFactory.makeButton("Išsaugoti", new OrbitSaveAction(this)));
    }

    public void submit() {
        this.x0ValidationLabel.setText("");
        this.aValidationLabel.setText("");
        this.cValidationLabel.setText("");
        this.fromValidationLabel.setText("");
        this.tillValidationLabel.setText("");

        boolean success = true;
        double x0 = 0, a = 0, c = 0;
        int from = 0, till = 0;

        try {
            x0 = Double.parseDouble(this.x0Field.getText());
        } catch (NumberFormatException e) {
            this.x0ValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            a = Double.parseDouble(this.aField.getText());
        } catch (NumberFormatException e) {
            this.aValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            c = Double.parseDouble(this.cField.getText());
        } catch (NumberFormatException e) {
            this.cValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            from = Integer.parseInt(this.fromField.getText());
        } catch (NumberFormatException e) {
            this.fromValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            till = Integer.parseInt(this.tillField.getText());
        } catch (NumberFormatException e) {
            this.tillValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        if (success && from < 0) {
            this.fromValidationLabel.setText("Turetu buti didesnis arba lygus nuliui!");
            success = false;
        }

        if (success && till < from) {
            this.tillValidationLabel.setText("Turetu buti didesnis arba lygus laukui nuo!");
            success = false;
        }

        Application.getInstance().getWindow().pack();
        if (!success)
            return;

        this.orbitConfiguration = new OrbitConfiguration(x0, a, c, from, till);
        this.orbitPanel.onConfigurationUpdate();
    }

    public OrbitConfiguration getOrbitConfiguration() {
        return orbitConfiguration;
    }
}

record OrbitSaveAction(OrbitConfigurePanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.submit();
    }
}
