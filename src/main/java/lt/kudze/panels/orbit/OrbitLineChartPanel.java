package lt.kudze.panels.orbit;

import lt.kudze.data.DynamicSystem;
import lt.kudze.data.OrbitConfiguration;
import lt.kudze.panels.OrangePanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.util.List;

public class OrbitLineChartPanel extends ChartPanel {

    protected static DefaultCategoryDataset initializeDataset(OrbitConfiguration configuration) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        List<Double> data = DynamicSystem.makeSeries(
                configuration.getA(),
                configuration.getC(),
                configuration.getX0(),
                configuration.getTill()
        );

        for (int i = configuration.getFrom(); i <= configuration.getTill(); i++)
            dataset.addValue(data.get(i), "", String.valueOf(i));

        return dataset;
    }

    protected static JFreeChart initializeChart(OrbitConfiguration configuration) {
        return ChartFactory.createLineChart(
                "Orbitu grafikas",
                "Iteracija",
                "Reikšmė",
                initializeDataset(configuration),
                PlotOrientation.VERTICAL,
                false,
                false,
                false
        );
    }

    public OrbitLineChartPanel(OrbitConfiguration configuration) {
        super(initializeChart(configuration));
    }

}
