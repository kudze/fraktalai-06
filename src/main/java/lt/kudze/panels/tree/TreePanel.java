package lt.kudze.panels.tree;

import lt.kudze.panels.GrayPanel;

public class TreePanel extends GrayPanel {
    protected TreeConfigurationPanel treeConfigurationPanel;

    public TreePanel() {
        this.treeConfigurationPanel = new TreeConfigurationPanel(this);

        this.addChild();
    }

    public void onConfigurationUpdate() {
        this.removeAll();
        this.addChild();

        this.revalidate();
        this.repaint();
    }

    protected void addChild() {
        this.add(treeConfigurationPanel);
        this.add(new TreeChartPanel(treeConfigurationPanel.getTreeConfiguration()));
    }

}
