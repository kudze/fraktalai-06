package lt.kudze.panels.tree;

import lt.kudze.Application;
import lt.kudze.components.RedLabel;
import lt.kudze.components.WhiteLabel;
import lt.kudze.data.TreeConfiguration;
import lt.kudze.factory.ButtonFactory;
import lt.kudze.panels.GrayPanel;
import lt.kudze.panels.InputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TreeConfigurationPanel extends GrayPanel {
    protected TreePanel treePanel;

    protected TreeConfiguration treeConfiguration = new TreeConfiguration();

    protected JTextField x0Field = new JTextField(String.valueOf(this.treeConfiguration.getX0()));
    protected JTextField aFromField = new JTextField(String.valueOf(this.treeConfiguration.getAFrom()));
    protected JTextField aTillField = new JTextField(String.valueOf(this.treeConfiguration.getATill()));
    protected JTextField cField = new JTextField(String.valueOf(this.treeConfiguration.getC()));

    protected JLabel x0ValidationLabel = new RedLabel("");
    protected JLabel aFromValidationLabel = new RedLabel("");
    protected JLabel aTillValidationLabel = new RedLabel("");
    protected JLabel cValidationLabel = new RedLabel("");

    public TreeConfigurationPanel(TreePanel treePanel) {
        this.treePanel = treePanel;

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(new WhiteLabel("Konfiguracija"));
        this.add(new InputPanel("x0", x0Field, x0ValidationLabel));
        this.add(new InputPanel("a nuo", aFromField, aFromValidationLabel));
        this.add(new InputPanel("a iki", aTillField, aTillValidationLabel));
        this.add(new InputPanel("c", cField, cValidationLabel));
        this.add(ButtonFactory.makeButton("Išsaugoti", new TreePanelSaveAction(this)));
    }

    public void submit() {
        this.x0ValidationLabel.setText("");
        this.aFromValidationLabel.setText("");
        this.aTillValidationLabel.setText("");
        this.cValidationLabel.setText("");

        boolean success = true;
        double x0 = 0, aFrom = 0, aTill = 0, c = 0;

        try {
            x0 = Double.parseDouble(this.x0Field.getText());
        } catch (NumberFormatException e) {
            this.x0ValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            aFrom = Double.parseDouble(this.aFromField.getText());
        } catch (NumberFormatException e) {
            this.aFromValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            aTill = Double.parseDouble(this.aTillField.getText());
        } catch (NumberFormatException e) {
            this.aTillValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            c = Double.parseDouble(this.cField.getText());
        } catch (NumberFormatException e) {
            this.cValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        if(success && aFrom > aTill) {
            this.aTillValidationLabel.setText("Turėtų būti didesnis arba lygus laukui: a nuo!");
            success = false;
        }

        Application.getInstance().getWindow().pack();
        if (!success)
            return;

        this.treeConfiguration = new TreeConfiguration(x0, aFrom, aTill, c);
        this.treePanel.onConfigurationUpdate();
    }

    public TreeConfiguration getTreeConfiguration() {
        return treeConfiguration;
    }
}

record TreePanelSaveAction(TreeConfigurationPanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.submit();
    }
}