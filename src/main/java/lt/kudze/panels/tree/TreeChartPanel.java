package lt.kudze.panels.tree;

import lt.kudze.data.DynamicSystem;
import lt.kudze.data.TreeConfiguration;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.util.List;

public class TreeChartPanel extends ChartPanel {
    protected static XYDataset initializeDataset(TreeConfiguration configuration) {
        XYSeries series = new XYSeries("");

        for (double a = configuration.getAFrom(); a <= configuration.getATill(); a += 0.00625) {
            List<Double> resList = DynamicSystem.makeSeries(
                    a,
                    configuration.getC(),
                    configuration.getX0(),
                    200
            );

            for (int i = 50; i <= 200; i++) {
                double res = resList.get(i);
                if (Double.isInfinite(res))
                    continue;

                series.add(a, res);
            }
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        return dataset;
    }

    protected static JFreeChart initializeChart(TreeConfiguration configuration) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Feigenbaumo medis",
                "A",
                "Ribiniai nejudami taškai",
                initializeDataset(configuration),
                PlotOrientation.VERTICAL,
                false,
                false,
                false
        );

        chart.getXYPlot().getDomainAxis().setRange(configuration.getAFrom(), configuration.getATill());

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, false);
        renderer.setSeriesShapesVisible(0, true);
        renderer.setSeriesShape(0, new Rectangle(1, 1));
        chart.getXYPlot().setRenderer(renderer);

        return chart;
    }

    public TreeChartPanel(TreeConfiguration configuration) {
        super(initializeChart(configuration));
    }
}
