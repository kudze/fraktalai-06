package lt.kudze.panels;

import java.awt.*;

public class GrayVerticalPanel extends GrayPanel {

    public GrayVerticalPanel() {
        super();

        this.setLayout(new GridLayout(0, 1, 0, 5));
    }
}
