package lt.kudze.panels.iterationPoint;

import lt.kudze.Application;
import lt.kudze.components.RedLabel;
import lt.kudze.components.WhiteLabel;
import lt.kudze.data.IterationPointConfiguration;
import lt.kudze.data.OrbitConfiguration;
import lt.kudze.factory.ButtonFactory;
import lt.kudze.panels.GrayPanel;
import lt.kudze.panels.InputPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IterationPointConfigurePanel extends GrayPanel {
    protected IterationPointPanel iterationPointPanel;
    protected IterationPointConfiguration iterationPointConfiguration = new IterationPointConfiguration();

    protected JTextField x0Field = new JTextField(String.valueOf(this.iterationPointConfiguration.getX0()));
    protected JTextField aField = new JTextField(String.valueOf(this.iterationPointConfiguration.getA()));
    protected JTextField cField = new JTextField(String.valueOf(this.iterationPointConfiguration.getC()));
    protected JTextField lineCountField = new JTextField(String.valueOf(this.iterationPointConfiguration.getLineCount()));

    protected JLabel x0ValidationLabel = new RedLabel("");
    protected JLabel aValidationLabel = new RedLabel("");
    protected JLabel cValidationLabel = new RedLabel("");
    protected JLabel lineCountValidationLabel = new RedLabel("");

    public IterationPointConfigurePanel(IterationPointPanel iterationPointPanel) {
        this.iterationPointPanel = iterationPointPanel;

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        this.add(new WhiteLabel("Konfiguracija"));
        this.add(new InputPanel("x0", x0Field, x0ValidationLabel));
        this.add(new InputPanel("a", aField, aValidationLabel));
        this.add(new InputPanel("c", cField, cValidationLabel));
        this.add(new InputPanel("linijų skaičius", lineCountField, lineCountValidationLabel));
        this.add(ButtonFactory.makeButton("Išsaugoti", new IterationPanelSaveAction(this)));
    }

    public void submit() {
        this.x0ValidationLabel.setText("");
        this.aValidationLabel.setText("");
        this.cValidationLabel.setText("");
        this.lineCountValidationLabel.setText("");

        boolean success = true;
        double x0 = 0, a = 0, c = 0;
        int lineCount = 0;

        try {
            x0 = Double.parseDouble(this.x0Field.getText());
        } catch (NumberFormatException e) {
            this.x0ValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            a = Double.parseDouble(this.aField.getText());
        } catch (NumberFormatException e) {
            this.aValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            c = Double.parseDouble(this.cField.getText());
        } catch (NumberFormatException e) {
            this.cValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        try {
            lineCount = Integer.parseInt(this.lineCountField.getText());
        } catch (NumberFormatException e) {
            this.lineCountValidationLabel.setText("Skaičius netinkamo formato!");
            success = false;
        }

        if(success && lineCount < 0) {
            this.lineCountValidationLabel.setText("Turėtų būti didesnis arba lygus nuliui!");
            success = false;
        }

        Application.getInstance().getWindow().pack();
        if (!success)
            return;

        this.iterationPointConfiguration = new IterationPointConfiguration(x0, a, c, lineCount);
        this.iterationPointPanel.onConfigurationUpdate();
    }

    public IterationPointConfiguration getIterationPointConfiguration() {
        return iterationPointConfiguration;
    }
}

record IterationPanelSaveAction(IterationPointConfigurePanel panel) implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        panel.submit();
    }
}
