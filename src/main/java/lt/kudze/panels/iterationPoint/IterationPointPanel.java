package lt.kudze.panels.iterationPoint;

import lt.kudze.panels.GrayPanel;

public class IterationPointPanel extends GrayPanel {
    protected IterationPointConfigurePanel iterationPointConfigurePanel;

    public IterationPointPanel() {
        this.iterationPointConfigurePanel = new IterationPointConfigurePanel(this);

        this.addChild();
    }

    public void onConfigurationUpdate() {
        this.removeAll();
        this.addChild();

        this.revalidate();
        this.repaint();
    }

    protected void addChild() {
        this.add(iterationPointConfigurePanel);
        this.add(new IterationPointChartPanel(iterationPointConfigurePanel.getIterationPointConfiguration()));
    }

    public IterationPointConfigurePanel getIterationPointConfigurePanel() {
        return iterationPointConfigurePanel;
    }
}
