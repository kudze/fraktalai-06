package lt.kudze.panels.iterationPoint;

import lt.kudze.data.DynamicSystem;
import lt.kudze.data.IterationPointConfiguration;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;

public class IterationPointChartPanel extends ChartPanel {
    protected static XYSeriesCollection computeAnalysisLines(IterationPointConfiguration configuration) {
        XYSeriesCollection dataset = new XYSeriesCollection();

        XYSeries analysis = new XYSeries("analysis_0");
        boolean state = false;

        double last = configuration.getX0();
        analysis.add(last, 0);
        for (int i = 0; i < configuration.getLineCount(); i++) {
            double nextX = last, nextY;

            if (state) {
                nextX = last;
                nextY = last;
            } else {
                nextY = DynamicSystem.calculate(
                        configuration.getA(),
                        configuration.getC(),
                        last
                );
            }

            analysis.add(nextX, nextY);
            dataset.addSeries(analysis);
            analysis = new XYSeries("analysis_" + (i + 1));
            analysis.add(nextX, nextY);

            state = !state;
            last = nextY;
        }
        dataset.addSeries(analysis);

        return dataset;
    }

    protected static XYSeriesCollection initializeDataset(IterationPointConfiguration configuration) {
        XYSeries yEqX = new XYSeries("y=x");
        yEqX.add(0, 0);
        yEqX.add(1, 1);

        XYSeries graph = new XYSeries("graph");
        for (double x = 0; x <= 1; x += 0.01)
            graph.add(x, DynamicSystem.calculate(
                    configuration.getA(),
                    configuration.getC(),
                    x
            ));

        XYSeriesCollection dataset = new XYSeriesCollection();
        for (Object series : computeAnalysisLines(configuration).getSeries())
            dataset.addSeries((XYSeries) series);

        dataset.addSeries(yEqX);
        dataset.addSeries(graph);
        return dataset;
    }

    protected static JFreeChart initializeChart(IterationPointConfiguration configuration) {
        XYSeriesCollection seriesCollection = initializeDataset(configuration);

        JFreeChart chart = ChartFactory.createXYLineChart(
                "Funkcijos iteruojamo taško grafinis vaizdas",
                "X",
                "Y",
                seriesCollection,
                PlotOrientation.VERTICAL,
                false,
                false,
                false
        );

        chart.getXYPlot().getDomainAxis().setRange(0, 2);
        chart.getXYPlot().getRangeAxis().setRange(0, 2);

        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < chart.getXYPlot().getSeriesCount(); i++) {
            renderer.setSeriesShapesVisible(i, false);

            if (seriesCollection.getSeries(i).getKey().toString().contains("analysis"))
                renderer.setSeriesPaint(i, Color.BLACK);
        }
        chart.getXYPlot().setRenderer(renderer);

        return chart;
    }

    public IterationPointChartPanel(IterationPointConfiguration configuration) {
        super(initializeChart(configuration));

    }

}
