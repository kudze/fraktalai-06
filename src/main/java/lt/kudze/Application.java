package lt.kudze;

import lt.kudze.panels.NavigationPanel;

public class Application {
    private static Application INSTANCE = null;

    public static Application getInstance() {
        return INSTANCE;
    }

    public static final String WINDOW_TITLE = "Karolis Kraujelis | Fraktalai 6";

    protected Window window;

    public Application() {
        INSTANCE = this;
        this.window = new Window(WINDOW_TITLE, new NavigationPanel());
    }

    public Window getWindow() {
        return window;
    }
}
