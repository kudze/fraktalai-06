# 06 Dinaminės sistemos simuliacija

Ši programa iš dinaminės sitemos gali nupiešti:

* orbitų grafiką
* funkcijos iteruojamo taško grafinį vaizdą
* feigenbaumo sritį

## Priklausomybės

1) Java JDK 19 versija
2) Maven

## Paleidimas

1) `mvn clean package` - Sukompiliuoja programa. (Sukompiliuota programa bus target aplanke).
2) `java -jar Fraktalai6-1.0-SNAPSHOT-jar-with-dependencies.jar` - Paleidžia programą.